﻿using System;
using System.Net;

namespace LyricsGetters {
    public class TimedWebClient : WebClient {
        private int timeout;
        public TimedWebClient(int t) {
            this.timeout = t;
        }

        /// <summary>
        /// Perform a get request from within the web client
        /// </summary>
        /// <param name="address">The address for the get request</param>
        /// <returns></returns>
        protected override WebRequest GetWebRequest(Uri address) {
            WebRequest w = base.GetWebRequest(address);
            w.Timeout = this.timeout;
            return w;
        }
    }
}
