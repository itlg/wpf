﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LyricsGetters.exceptions {
    class LyricsNotFoundException : Exception {

    }

    class LyricsRequestTimedOutException :  Exception {

    }

    class GenericWebErrorException : Exception {

    }
}
