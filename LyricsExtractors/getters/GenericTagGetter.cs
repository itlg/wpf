﻿using LyricsGetters.exceptions;
using LyricsGetters.interfaces;
using System;
using System.Net;

namespace LyricsGetters.getters {
    /// <summary>
    /// Get the lyrics as surrounded by two html tags
    /// </summary>
    class GenericTagGetter : LyricsGetter {
        private int mTimeout;
        private string mStartTag;
        private string mEndTag;
        private Func<String, String, String, String, String> mUrlGenerator;

        /// <summary>
        /// Constructor of the getter
        /// </summary>
        /// <param name="start_tag"></param>
        /// <param name="end_tag"></param>
        /// <param name="urlGenerator">A function that takes (Song, Artist, Album, Year) and return the url to which download lyrics</param>
        /// <param name="timeout"></param>
        public GenericTagGetter(string start_tag, string end_tag, Func<String, String, String, String, String> url_generator, int timeout) {
            this.mStartTag = start_tag;
            this.mEndTag = end_tag;
            this.mTimeout = timeout;
            this.mUrlGenerator = url_generator;
        }

        /// <summary>
        /// Download the web page
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        string GetWebPage(String url) {
            string htmlCode;
            using (TimedWebClient client = new TimedWebClient(mTimeout)) {
                try {
                    htmlCode = client.DownloadString(url).ToString();
                }
                catch (WebException e) {
                    // Check 404 errors
                    if ((e.Response is System.Net.HttpWebResponse) &&
                        (e.Response as System.Net.HttpWebResponse).StatusCode == System.Net.HttpStatusCode.NotFound)
                        throw new LyricsNotFoundException();
                    // Check if request timed out
                    if (e.Status == WebExceptionStatus.Timeout)
                        throw new LyricsRequestTimedOutException();
                    // Otherwise
                    throw new GenericWebErrorException();
                }
            }
            return htmlCode;
        }

        /// <summary>
        /// Extract the lyrics from the html the passed a regex
        /// </summary>
        /// <param name="html"></param>
        /// <returns></returns>
        string IsolateLyrics(string html) {
            if (html.IndexOf(mStartTag) > 0) {
                int c = html.IndexOf(mStartTag);
                html = html.Remove(0, c);
            }
            else throw new LyricsNotFoundException();
            html = html.Remove(html.IndexOf(mEndTag));
            return html.Trim();
        }

        public string GetLyrics(string song, string artist, string album, string year) {
            return IsolateLyrics(GetWebPage(this.mUrlGenerator(song, artist, album, year)));
        }
    }
}
