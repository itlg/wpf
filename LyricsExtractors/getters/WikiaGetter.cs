﻿using LyricsGetters.interfaces;
using System;
using System.Text.RegularExpressions;

namespace LyricsGetters.getters {
    /// <summary>
    /// This is the lyrics extractor for the Wikia portal. It implements a GenericTagGetter
    /// </summary>
    class WikiaGetter : LyricsGetter {

        private int mTimeout;
        private GenericTagGetter mTagGetter;

        public WikiaGetter(int timeout) {
            this.mTimeout = timeout;
            Func<String, String, String, String, String> urlGenerator = (song, artist, album, year) => "http://lyrics.wikia.com/" + artist + ":" + song;
            string start_tag = "<div class='lyricbox'>";
            string end_tag = "<div class='lyricsbreak'>";
            mTagGetter = new GenericTagGetter(start_tag, end_tag, urlGenerator, mTimeout);
        }

        public string GetLyrics(string song, string artist, string album, string year) {
            return mTagGetter.GetLyrics(song, artist, album, year);
        }
    }
}
