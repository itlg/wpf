﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LyricsGetters.interfaces {
    interface LyricsGetter {

        /// <summary>
        /// Download the lyrics for a particular song given its metadata
        /// </summary>
        /// <param name="artist"></param>
        /// <param name="song"></param>
        /// <param name="album"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        string GetLyrics(string song, string artist, string album, string year);

    }
}
