﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;

namespace LyricsGetters.exceptions {
    public class GenericWebErrorException : Exception {
        private WebException mWebEx;
        public GenericWebErrorException(WebException e) {
            this.mWebEx = e;
        }

        public WebException GetWebException() { return this.mWebEx; }

        public override String ToString() {
            return String.Format("status={0} message=\"{1}\" response=\"{2}\"", this.mWebEx.Status, this.mWebEx.Message, this.mWebEx.Response);
        }
    }

    public class LyricNotFoundException : Exception {

    }

    public class LyricsRequestTimedOutException : Exception {

    }
}
