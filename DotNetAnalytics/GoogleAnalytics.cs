﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using DotNetHttp;

namespace DotNetAnalytics {
    public class GoogleAnalyticsManager {

        private const string API_URL = "https://www.google-analytics.com/collect";

        private const string QUERY_TAG_VERSION = "v";
        private const string QUERY_TAG_PROPERTY_ID = "tid";
        private const string QUERY_TAG_ANONYMOUS_CLIENT_ID = "cid";
        private const string QUERY_TAG_HIT_TYPE = "t";

        private const string QUERY_TYPE_EVENT_VALUE = "event";
        private const string QUERY_TAG_EVENT_CATEGORY = "ec";
        private const string QUERY_TAG_EVENT_ACTION = "ea";
        private const string QUERY_TAG_EVENT_LABEL = "el";
        private const string QUERY_TAG_EVENT_VALUE = "ev";

        private const string QUERY_TYPE_EXCEPTION_VALUE = "exception";
        private const string QUERY_TAG_EXCEPTION_DESCRIPTION = "exd";
        private const string QUERY_TAG_EXCEPTION_FATAL = "exf";

        private const string QUERY_TYPE_PAGE_VALUE = "pageview";
        private const string QUERY_TAG_PAGE_HOSTNAME = "dh";
        private const string QUERY_TAG_PAGE_PAGE = "dp";
        private const string QUERY_TAG_PAGE_TITLE = "dt";

        private readonly string mUaid;
        private readonly string mAnonymousId;

        public GoogleAnalyticsManager(string uaid, string anonymousClientId) {
            this.mUaid = uaid;
            this.mAnonymousId = anonymousClientId;
        }

        private Dictionary<string, string> GetRequiredQueryParameters() {
            var dict = new Dictionary<string, string> {
                { QUERY_TAG_VERSION, "1" },
                { QUERY_TAG_PROPERTY_ID, this.mUaid },
                { QUERY_TAG_ANONYMOUS_CLIENT_ID, this.mAnonymousId }
            };
            return dict;
        }

        /// <summary>
        /// Logs and event, as described in https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide#event
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="eventCategory"></param>
        /// <param name="eventAction"></param>
        /// <param name="eventValue"></param>
        /// <returns></returns>
        public async Task<string> LogEvent(string eventCategory, string eventAction, string eventLabel, uint eventValue) {
            var queryDict = GetRequiredQueryParameters();
            queryDict.Add(QUERY_TAG_HIT_TYPE, QUERY_TYPE_EVENT_VALUE);
            queryDict.Add(QUERY_TAG_EVENT_CATEGORY, eventCategory);
            queryDict.Add(QUERY_TAG_EVENT_ACTION, eventAction);
            queryDict.Add(QUERY_TAG_EVENT_LABEL, eventLabel);
            queryDict.Add(QUERY_TAG_EVENT_VALUE, eventValue.ToString());
            return await Http.getInstance().GetAsync(API_URL, queryDict);
        }

        /// <summary>
        /// Logs and exception, as described in https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide#exception
        /// </summary>
        /// <param name="exceptionName"></param>
        /// <param name="fatal"></param>
        /// <returns></returns>
        public async Task<string> LogException(string exceptionDescription, bool exceptionFatal) {
            var queryDict = GetRequiredQueryParameters();
            queryDict.Add(QUERY_TAG_HIT_TYPE, QUERY_TYPE_EXCEPTION_VALUE);
            queryDict.Add(QUERY_TAG_EXCEPTION_DESCRIPTION, exceptionDescription);
            queryDict.Add(QUERY_TAG_EXCEPTION_FATAL, exceptionFatal ? "0" : "1");
            return await Http.getInstance().GetAsync(API_URL, queryDict);
        }

        /// <summary>
        /// Logs a page view, as described in https://developers.google.com/analytics/devguides/collection/protocol/v1/devguide#page
        /// </summary>
        /// <param name="exceptionName"></param>
        /// <param name="fatal"></param>
        /// <returns></returns>
        public async Task<string> LogPageView(string hostname, string page, string title) {
            var queryDict = GetRequiredQueryParameters();
            queryDict.Add(QUERY_TAG_HIT_TYPE, QUERY_TYPE_PAGE_VALUE);
            queryDict.Add(QUERY_TAG_PAGE_HOSTNAME, hostname);
            queryDict.Add(QUERY_TAG_PAGE_PAGE, page);
            queryDict.Add(QUERY_TAG_PAGE_TITLE, title);
            return await Http.getInstance().GetAsync(API_URL, queryDict);
        }
    }
}
