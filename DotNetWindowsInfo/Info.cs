﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace DotNetWindowsInfo {

    public static class SystemInfo {
        private const string REG_KEY = @"SOFTWARE\Microsoft\Windows NT\CurrentVersion\";

        public static models.WindowsInfo GetInfo() {
            using (var ndpKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32).OpenSubKey(REG_KEY)) {
                if (ndpKey != null) {
                    string prodname = "";
                    uint currentbuild = 0;
                    string releaseid = "";
                    string editionid = "";

                    if (ndpKey.GetValue("ProductName") != null)
                        prodname = (string)ndpKey.GetValue("ProductName");
                    if (ndpKey.GetValue("ReleaseId") != null)
                        releaseid = (string)ndpKey.GetValue("ReleaseId");
                    if (ndpKey.GetValue("EditionID") != null)
                        editionid = (string)ndpKey.GetValue("EditionID");
                    if (ndpKey.GetValue("CurrentBuild") != null) {
                        string currentbuild_tmp = (string)ndpKey.GetValue("CurrentBuild");
                        currentbuild = UInt32.Parse(currentbuild_tmp);
                    }
                    return new models.WindowsInfo(prodname, currentbuild, releaseid, editionid);
                }
                return null;
            }
        }
    }
}
