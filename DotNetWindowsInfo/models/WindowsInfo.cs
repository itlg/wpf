﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DotNetWindowsInfo.models {
    public class WindowsInfo {
        public string ProductName { get; }
        public uint CurrentBuild { get; }
        public string ReleaseId { get; }
        public string EditionId { get; }

        public WindowsInfo(string prodname, uint build, string releaseid, string editionid) {
            this.ProductName = prodname;
            this.CurrentBuild = build;
            this.ReleaseId = releaseid;
            this.EditionId = editionid;
        }

        public override string ToString() {
            return $"{this.ProductName}::{this.ReleaseId}::{this.CurrentBuild}";
        }

        public string ToUserAgentString() {
            return $"{this.ProductName}/{this.ReleaseId}/{this.CurrentBuild}";
        }
    }
}
