﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTunesLib;
using System.Threading;
using System.Runtime.InteropServices;
using System.IO;
using System.Windows.Controls;

namespace iLyricsGrabber.workers {
    internal class NativeMethods {
        // ======================================================================= START Mute Sounds
        public const int FEATURE_DISABLE_NAVIGATION_SOUNDS = 21;
        public const int SET_FEATURE_ON_THREAD = 0x00000001;
        public const int SET_FEATURE_ON_PROCESS = 0x00000002;
        public const int SET_FEATURE_IN_REGISTRY = 0x00000004;
        public const int SET_FEATURE_ON_THREAD_LOCALMACHINE = 0x00000008;
        public const int SET_FEATURE_ON_THREAD_INTRANET = 0x00000010;
        public const int SET_FEATURE_ON_THREAD_TRUSTED = 0x00000020;
        public const int SET_FEATURE_ON_THREAD_INTERNET = 0x00000040;
        public const int SET_FEATURE_ON_THREAD_RESTRICTED = 0x00000080;

        [DllImport("urlmon.dll")]
        [PreserveSig]
        [return: MarshalAs(UnmanagedType.Error)]
        public static extern int CoInternetSetFeatureEnabled(int FeatureEntry, [MarshalAs(UnmanagedType.U4)] int dwFlags, bool fEnable);
        // ======================================================================= END Mute Sounds
    }

    class SongLyricsFetcherWorker : IDisposable{


        private IITFileOrCDTrack track;
        private int song_index;
        WebBrowser webBrowser;                                              // It's used to hack the lyrics on the page
        // Main main_form = (Main)Application.OpenForms["Main"];

        public SongLyricsFetcherWorker(IITFileOrCDTrack track, int song_index) {
            this.track = track;
            this.song_index = song_index;
            // Mute Sounds
            int feature = NativeMethods.FEATURE_DISABLE_NAVIGATION_SOUNDS;
            NativeMethods.CoInternetSetFeatureEnabled(feature, NativeMethods.SET_FEATURE_ON_PROCESS, true);
        }

        public void DoWork() {
            utils.Analytics.GetInstance().DownloadLyricsStarted();
            // Instantiate the extractor
            LyricsGetters.WikiaExtractor wikiaExtractor = new LyricsGetters.WikiaExtractor(Properties.Settings.Default.WebRequestTimeOut);
            // Request lyrics and if found set them to the song
            try {
                // TODO replace calling of main_methods with message handling
                String htmlCode = wikiaExtractor.getLyrics(track.Name, track.Artist);
                // main_form.updateStatusForSong(track.Name, track.Artist, song_index, Main.STATUS_LYRICS_FOUND);
                // Decrypt the lyrics
                this.webBrowser = new WebBrowser();
                // this.webBrowser.ScriptErrorsSuppressed = true;
                // In the constructor does not work
                displayHtml(string.Format("<html><body><p>{0}</p></body></html>", htmlCode));
                // Try to set Lyrics to track
                try {
                    // track.Lyrics = webBrowser.Document.Body.InnerText.Trim();
                    // main_form.updateStatusForSong(track.Name, track.Artist, song_index, Main.STATUS_COMPLETED);
                    utils.Analytics.GetInstance().DownloadLyricsSuccess();
                } catch (COMException) {
                    // main_form.updateStatusForSong(track.Name, track.Artist, song_index, Main.STATUS_COMERROR);
                    LyricsFetcherWorker.incrementErrorsTrackCount();
                    utils.Analytics.GetInstance().DownloadLyricsError();
                    // utils.Analytics.GetInstance().Exception(Main.STATUS_COMERROR);
                } catch (Exception) {
                    // Catch a generic error when sending lyrics to iTunes
                    // main_form.updateStatusForSong(track.Name, track.Artist, song_index, Main.STATUS_SENDING_LYRICS_ERROR);
                    LyricsFetcherWorker.incrementErrorsTrackCount();
                    utils.Analytics.GetInstance().DownloadLyricsError();
                    // utils.Analytics.GetInstance().Exception(Main.STATUS_SENDING_LYRICS_ERROR);
                } finally {
                    this.webBrowser.Dispose();
                }
            } catch (LyricsGetters.exceptions.LyricNotFoundException) {
                // main_form.updateStatusForSong(track.Name, track.Artist, song_index, Main.STATUS_LYRICS_NOT_FOUND);
                LyricsFetcherWorker.incrementNotFoundTrackCount();
                // utils.Analytics.GetInstance().Exception(Main.STATUS_LYRICS_NOT_FOUND);
            } catch (LyricsGetters.exceptions.LyricsRequestTimedOutException) {
                // main_form.updateStatusForSong(track.Name, track.Artist, song_index, Main.STATUS_TIMEOUT);
                LyricsFetcherWorker.incrementErrorsTrackCount();
                utils.Analytics.GetInstance().DownloadLyricsError();
                // utils.Analytics.GetInstance().Exception(Main.STATUS_TIMEOUT);
            } catch (LyricsGetters.exceptions.GenericWebErrorException e) {
                // main_form.updateStatusForSong(track.Name, track.Artist, song_index, Main.STATUS_WEBERROR, e.ToString());
                LyricsFetcherWorker.incrementErrorsTrackCount();
                utils.Analytics.GetInstance().DownloadLyricsError();
                // utils.Analytics.GetInstance().Exception(Main.STATUS_WEBERROR);
            } catch (ThreadAbortException) {
                // if(this.webBrowser != null && !this.webBrowser.IsDisposed && this.webBrowser.IsBusy) this.webBrowser.Stop();
                // main_form.updateStatusForSong(track.Name, track.Artist, song_index, Main.STATUS_ABORTED);
                utils.Analytics.GetInstance().DownloadLyricsError();
                // utils.Analytics.GetInstance().Exception(Main.STATUS_ABORTED);
            }
        }

        private void displayHtml(string html) {
            webBrowser.Navigate("about:blank");
            if (webBrowser.Document != null) {
                // webBrowser.Document.Write(string.Empty);
            }
            // webBrowser.Document.OpenNew(true);
            // webBrowser.Document.Write(html);
        }

        public void Dispose() {
            webBrowser.Dispose();
        }
    }
}