﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace iLyricsGrabber.models {
    class Track {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
        public int TunesId { get; set; }
        public string Title { get; set; }
        public string Artist { get; set; }
    }
}
